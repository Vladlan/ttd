import {addTwoNumbers, ListNode} from "./add-two-numbers";

describe("show sum two integers", () => {
    test("l1 = [2,4,3], l2 = [5,6,4] = [7,0,8]", () => {
        const l1 = new ListNode(2, new ListNode(4, new ListNode(3, null)));
        const l2 = new ListNode(5, new ListNode(6, new ListNode(4, null)));
        const res = addTwoNumbers(l1, l2);
        const res2 = new ListNode(7, new ListNode(0, new ListNode(8, null)));
        expect(res.val).toEqual(res2.val);
        expect(res.next.val).toEqual(res2.next.val);
    });
});
