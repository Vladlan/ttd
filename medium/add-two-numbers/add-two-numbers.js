// Definition for singly-linked list.
export function ListNode(val, next) {
    this.val = (val === undefined ? 0 : val)
    this.next = (next === undefined ? null : next)
}

/**
 * @param {number[]} l1
 * @param {number[]} l2
 * @return {number[]}
 */
export const addTwoNumbers = (l1, l2) => {
    let startNode = new ListNode(0);
    let ll1 = l1;
    let ll2 = l2;
    let carry = 0;
    let curr = startNode;
    while (ll1 || ll2) {
        let x = !!ll1 ? ll1.val : 0;
        let y = !!ll2 ? ll2.val : 0;
        let sum = carry + x + y;
        carry = Math.floor(sum / 10);
        curr.next = new ListNode(Math.floor(sum % 10));
        curr = curr.next;
        if (ll1) ll1 = ll1.next;
        if (ll2) ll2 = ll2.next;
    }
    if (carry > 0) {
        curr.next = new ListNode(carry);
    }
    return startNode.next;
};
