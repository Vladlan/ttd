import { divide } from "./divide-two-integers";

describe("divide two integers function (without divide, mod, and multiple operators)", () => {
    test("8 / 3 = 2", () => {
        expect(divide(8,3)).toBe(2);
    });
    test("8 / -3 = -2", () => {
        expect(divide(8,-3)).toBe(-2);
    });
    test("-8 / -3 = 2", () => {
        expect(divide(-8,-3)).toBe(2);
    });
    test("-8 / 3 = -2", () => {
        expect(divide(-8,3)).toBe(-2);
    });
    test("-8 / 2 = -4", () => {
        expect(divide(-8,2)).toBe(-4);
    });
    test("7 / -3 = -2", () => {
        expect(divide(7,-3)).toBe(-2);
    });
    test("1 / 1 = 1", () => {
        expect(divide(1,1)).toBe(1);
    });
    test("0 / 0 = 0", () => {
        expect(divide(1,1)).toBe(1);
    });
    test("0 / 1 = 1", () => {
        expect(divide(0,1)).toBe(0);
    });
    test("-2147483648 / -1 = 2147483647", () => {
        expect(divide(-2147483648,-1)).toBe(2147483647);
    });
    test("-2147483648/ 1 = 2147483648", () => {
        expect(divide(-2147483648,1)).toBe(-2147483648);
    });
});
