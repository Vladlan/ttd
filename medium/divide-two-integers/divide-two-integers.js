/**
 * @param {number} dividend
 * @param {number} divisor
 * @return {number}
 */
export const divide = (dividend, divisor) => {
    if (1 === Math.abs(divisor)) return getSafeRes(dividend * divisor);
    let sum = Math.abs(divisor);
    let absDividend = Math.abs(dividend);
    const absDivisor = Math.abs(divisor);
    let counter = 0;
    while (absDividend >= sum) {
        sum = sum + absDivisor;
        counter++;
    }
    const sign = dividend * divisor > 0 ? 1 : -1;
    return getSafeRes(sign * counter);
};

function getSafeRes(res) {
    let safeRes = res <= -2147483648 ? -2147483648 : res;
    safeRes = safeRes >= 2147483647 ? 2147483647 : safeRes;
    return safeRes
}
